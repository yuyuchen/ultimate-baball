# Ultimate Baball

Ce jeu a été réalisé dans le cadre d'un project d'école (Supinfo), le but est de redévelopper le jeu Kula World en 3D. 

Une explication entière du projet se trouve dans `sujet.pdf`.

Déveloper avec Unity 3D, en 2018-2019.

## Lancer le jeu
Executez `UltimateBaball.exe` qui se trouve dans le dossier `UltimateBaball-Game`.

Si vous voulez juste lancer le jeu, téléchargez seulement le dossier `UltimateBaball-Game`, car le code source (`CodeSourceUnityProject.zip`) est très lourd à télécharger (près de 2Go). 

## Démo 
Vous pouvez trouvez une démo du jeu via ce [lien](https://drive.google.com/file/d/18jJH8pfMeqAoBLYFptqJLKTSPIx2lZSN/view?usp=sharing)


## Comprendre le jeu 
* Pour voir les règles du jeu : `user-documentation.pdf`
* Pour comprendre les scripts unity du jeu: `technical-documentation.pdf`

## Code source 
Déziper le fichier `CodeSourceUnityProject.zip`, puis l'ouvrir avec Unity.

